package com.giphy.viewer.data.utils

import com.giphy.viewer.data.DataConfig
import com.giphy.viewer.data.models.ImageDto
import com.google.gson.*
import java.lang.reflect.Type

/**
 * Created by Natalia on 01.10.2018.
 */
class SingleImageDeserializer(private val gson: Gson) : JsonDeserializer<ImageDto> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ImageDto? {
        val jsonObject: JsonObject = json as JsonObject
        val content = jsonObject.get(DataConfig.FIELD_DATA).asJsonObject
        return gson.fromJson(content, typeOfT)
    }
}