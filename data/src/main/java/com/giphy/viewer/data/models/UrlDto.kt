package com.giphy.viewer.data.models

/**
 * Created by Natalia on 01.10.2018.
 */
class UrlDto(val url: String)