package com.giphy.viewer.data.mappers

import com.giphy.viewer.data.models.ImageResponse
import com.giphy.viewer.domain.models.Page
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.Function

/**
 * Created by Natalia on 02.10.2018.
 */
class PageConverter(private val imageConverter: ImageConverter) : Function<ImageResponse, Single<Page>> {
    override fun apply(response: ImageResponse): Single<Page> {
        return Flowable.fromIterable(response.images)
                .map { imageConverter.convert(it) }
                .toList()
                .map { Page(it, response.totalCount) }
    }
}