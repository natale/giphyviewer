package com.giphy.viewer.data

object DataConfig {
    const val API_URL = "https://api.giphy.com"
    const val GIPHY_API_KEY = "Uvt7qjMhDoK28C8FA7sTnnt8DbM5g18b"
    const val OFFSET = "offset"
    const val LIMIT = "limit"
    const val API_KEY = "api_key"
    const val REQUIRES_API_KEY =  "Requires-Key: true"

    const val FIELD_DATA = "data"
    const val FIELD_URL = "url"
    const val FIELD_FIXED_WIDTH_STILL = "fixed_width_still"
    const val FIELD_PAGINATION = "pagination"
    const val FIELD_TOTAL_COUNT = "total_count"
}