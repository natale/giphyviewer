package com.giphy.viewer.data.utils

import com.giphy.viewer.data.DataConfig
import com.giphy.viewer.data.models.UrlDto
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.lang.reflect.Type

/**
 * Created by Natalia on 01.10.2018.
 */
class UrlDeserializer : JsonDeserializer<UrlDto> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): UrlDto? {
        val jsonObject: JsonObject = json as JsonObject
        val urlObject = jsonObject.get(DataConfig.FIELD_FIXED_WIDTH_STILL).asJsonObject
        val url = urlObject.get(DataConfig.FIELD_URL).asString
        return UrlDto(url)
    }
}