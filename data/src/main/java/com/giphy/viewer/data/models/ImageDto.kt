package com.giphy.viewer.data.models

import com.google.gson.annotations.SerializedName

class ImageDto(
        @SerializedName("images")
        val urlDto: UrlDto,
        @SerializedName("user")
        val user: UserDto?
)