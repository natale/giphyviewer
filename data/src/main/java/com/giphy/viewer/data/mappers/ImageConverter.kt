package com.giphy.viewer.data.mappers

import com.giphy.viewer.data.models.ImageDto
import com.giphy.viewer.data.models.UserDto
import com.giphy.viewer.domain.models.Image
import com.giphy.viewer.domain.models.User

/**
 * Created by natalia on 28.09.2018.
 */
class ImageConverter {

    companion object {
        const val EMPTY_STRING = ""
    }

    fun convert(from: ImageDto): Image {
        val url = from.urlDto.url
        return Image(url, convert(from.user))
    }

    private fun convert(from: UserDto?): User = User(
            name = from?.userName.defaultIfNull(),
            displayName = from?.displayName.defaultIfNull(),
            profileUrl = from?.profileUrl.defaultIfNull(),
            twitter = from?.twitter.defaultIfNull())


    fun String?.defaultIfNull(): String = this ?: EMPTY_STRING
}