package com.giphy.viewer.data.repositories

import com.giphy.viewer.data.api.GiphyApi
import com.giphy.viewer.data.mappers.ImageConverter
import com.giphy.viewer.data.mappers.PageConverter
import com.giphy.viewer.domain.models.Image
import com.giphy.viewer.domain.models.Page
import com.giphy.viewer.domain.repositories.ImagesRepository
import io.reactivex.Single

/**
 * Created by natalia on 28.09.2018.
 */
class ImagesRepositoryImpl(private val api: GiphyApi,
                           private val imageConverter: ImageConverter,
                           private val pageConverter: PageConverter) : ImagesRepository {

    private var maxImageCount = Int.MAX_VALUE

    override fun loadImages(offset: Int, count: Int): Single<Page> =
            api.getTrendingImages(offset, count)
                    .doOnSuccess { maxImageCount = it.totalCount }
                    .flatMap { pageConverter.apply(it) }

    override fun loadImage(id: String): Single<Image> =
            api.getImage(id)
                    .map { imageConverter.convert(it) }

    override fun getMaxImagesCount(): Int = maxImageCount
}