package com.giphy.viewer.data.models

/**
 * Created by Natalia on 02.10.2018.
 */
class ImageResponse(
    val images: List<ImageDto>,
    val totalCount: Int
)