package com.giphy.viewer.data.api

import com.giphy.viewer.data.DataConfig
import com.giphy.viewer.data.DataConfig.API_KEY
import com.giphy.viewer.data.DataConfig.GIPHY_API_KEY
import com.giphy.viewer.data.DataConfig.REQUIRES_API_KEY
import okhttp3.Interceptor
import okhttp3.Response


/**
 * Created by Natalia on 01.10.2018.
 */
class KeyInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val keyRequired = request.headers().names().contains(REQUIRES_API_KEY.split(":")[0])
        if (keyRequired) {
            val url = request.url().newBuilder()
                    .addQueryParameter(API_KEY, GIPHY_API_KEY)
                    .build()
            request = request.newBuilder()
                    .removeHeader(DataConfig.REQUIRES_API_KEY)
                    .url(url)
                    .build()
        }
        return chain.proceed(request)
    }
}