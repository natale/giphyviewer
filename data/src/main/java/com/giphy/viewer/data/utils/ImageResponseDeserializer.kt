package com.giphy.viewer.data.utils

import com.giphy.viewer.data.DataConfig.FIELD_DATA
import com.giphy.viewer.data.DataConfig.FIELD_PAGINATION
import com.giphy.viewer.data.DataConfig.FIELD_TOTAL_COUNT
import com.giphy.viewer.data.models.ImageDto
import com.giphy.viewer.data.models.ImageResponse
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * Created by Natalia on 01.10.2018.
 */
class ImageResponseDeserializer(private val gson: Gson) : JsonDeserializer<ImageResponse> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ImageResponse? {
        val jsonObject: JsonObject = json as JsonObject

        val imageListType = object : TypeToken<List<@JvmSuppressWildcards ImageDto>>() {}.type

        val content = jsonObject.get(FIELD_DATA).asJsonArray
        val images: List<ImageDto> = gson.fromJson(content, imageListType)

        val pagination = jsonObject.get(FIELD_PAGINATION).asJsonObject
        val maxCount = pagination.get(FIELD_TOTAL_COUNT).asInt
        return ImageResponse(images, maxCount)
    }
}