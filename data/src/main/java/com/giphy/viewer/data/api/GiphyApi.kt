package com.giphy.viewer.data.api

import com.giphy.viewer.data.DataConfig.LIMIT
import com.giphy.viewer.data.DataConfig.OFFSET
import com.giphy.viewer.data.DataConfig.REQUIRES_API_KEY
import com.giphy.viewer.data.models.ImageDto
import com.giphy.viewer.data.models.ImageResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface GiphyApi {
    @Headers("api_key: Uvt7qjMhDoK28C8FA7sTnnt8DbM5g18b")
    @GET("/v1/gifs/trending")
    fun getTrendingImages(@Query(OFFSET) offset: Int,
                          @Query(LIMIT) limit: Int): Single<ImageResponse>

    @Headers(REQUIRES_API_KEY)
    @GET("v1/gifs/{id}")
    fun getImage(@Path("id") id: String): Single<ImageDto>
}