package com.giphy.viewer.data.models

import com.google.gson.annotations.SerializedName

class UserDto(
    @SerializedName("username")
    val userName: String?,

    @SerializedName("profile_url")
    val profileUrl: String?,

    @SerializedName("display_name")
    val displayName: String?,

    @SerializedName("twitter")
    val twitter: String?
)