package com.giphy.viewer.presenter.base

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by natalia on 29.09.2018.
 */
abstract class BasePresenter<V : MvpView> : MvpPresenter<V>() {

    private val disposables = CompositeDisposable()

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    protected fun removeDisposable(disposable: Disposable) {
        disposables.remove(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}