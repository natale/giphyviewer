package com.giphy.viewer.presenter.gallery

import com.arellomobile.mvp.InjectViewState
import com.giphy.viewer.R
import com.giphy.viewer.domain.business.ImageInteractor
import com.giphy.viewer.presenter.base.BasePresenter
import com.giphy.viewer.utils.ImageViewModelConverter
import com.giphy.viewer.utils.ItemClickListener
import com.giphy.viewer.utils.recycler.RecyclerViewItem
import com.giphy.viewer.utils.resources.StringManager
import com.giphy.viewer.view.gallery.GalleryView
import com.giphy.viewer.view.gallery.recycler.ErrorItem
import com.giphy.viewer.view.gallery.recycler.ImageItem
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by natalia on 29.09.2018.
 */
@InjectViewState
class GalleryPresenter @Inject constructor(private val interactor: ImageInteractor,
                                           private val stringManager: StringManager,
                                           private val converter: ImageViewModelConverter)
    : BasePresenter<GalleryView>(), ItemClickListener<RecyclerViewItem> {

    private var isLoading: Boolean = false
    private var currentOffset: Int = 0

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        reloadImages()
    }

    override fun onItemClick(item: RecyclerViewItem) {
        when (item) {
            is ImageItem -> viewState.openImageDetails(item.image)
            is ErrorItem -> onRetryLoadNextPages()
        }
    }

    private fun onRetryLoadNextPages() {
        //viewState.showListLoading()
        loadNextImages(currentOffset)
    }

    fun onRetryRefreshClick() {
        reloadImages()
    }

    fun onScrolled(visibleItemCount: Int, totalItemCount: Int, pastVisibleItems: Int) {
        if (visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {

            loadNextImages(totalItemCount)
        }
    }

    fun onRefresh() {
        reloadImages()
    }

    private fun loadNextImages(offset: Int) {
        addDisposable(getLoadingObservable(offset)
                .doOnSubscribe { viewState.showListLoading() }
                .subscribe({ showImages(it) },
                        { _ -> viewState.showListError(getErrorMessage()) }))
    }

    private fun reloadImages() {
        addDisposable(getLoadingObservable(0)
                .doOnSubscribe { viewState.showRefreshing() }
                .subscribe({ refreshImages(it) },
                        { _ -> viewState.showRefreshError(getErrorMessage()) }))
    }

    private fun showImages(list: List<ImageItem>) {
        viewState.hideLoading()
        viewState.showImages(list)
    }

    private fun refreshImages(list: List<ImageItem>) {
        viewState.hideLoading()
        viewState.updateImages(list)
    }

    private fun getErrorMessage() = stringManager.getString(R.string.default_error_message)

    private fun getLoadingObservable(offset: Int) =
            interactor.loadImages(offset)
                    .map { converter.convert(it) }
                    .doOnSubscribe { isLoading = true }
                    .doOnSubscribe { currentOffset = offset }
                    .doOnSuccess { isLoading = false }
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { viewState.hideError() }
                    .doOnError { _ -> viewState.hideLoading() }
                    .doOnError { e -> Timber.e(e.message) }
}