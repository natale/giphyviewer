package com.giphy.viewer.presenter.image

import com.arellomobile.mvp.InjectViewState
import com.giphy.viewer.R
import com.giphy.viewer.domain.business.ImageInteractor
import com.giphy.viewer.models.ImageViewModel
import com.giphy.viewer.presenter.base.BasePresenter
import com.giphy.viewer.utils.ImageViewModelConverter
import com.giphy.viewer.utils.resources.StringManager
import com.giphy.viewer.view.image.SingleImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by Natalia on 30.09.2018.
 */
@InjectViewState
class ImagePresenter @Inject constructor(private val imageInteractor: ImageInteractor,
                                         private val converter: ImageViewModelConverter,
                                         private val stringManager: StringManager) : BasePresenter<SingleImageView>() {

    var image: ImageViewModel? = null
    var deepLinkImageId: String? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        image?.let { viewState.showImageDetails(it) }
        deepLinkImageId?.let { loadImageDetails(it) }
    }

    fun onUrlClick() {
        image?.let { viewState.openUrl(it.profileUrl) }
    }

    fun onRetryClick() {
        deepLinkImageId?.let { loadImageDetails(it) }
    }

    private fun loadImageDetails(url: String) {
        addDisposable(imageInteractor.loadImage(url)
                .map { converter.convertImage(it) }
                .map { it.image }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showLoading() }
                .doFinally { viewState.hideLoading() }
                .subscribe({ viewState.showImageDetails(it) },
                        { _ -> viewState.showError(getErrorMessage()) })
        )
    }

    private fun getErrorMessage() = stringManager.getString(R.string.default_error_message)
}