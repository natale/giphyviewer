package com.giphy.viewer.di.modules

import com.giphy.viewer.domain.business.ImageInteractor
import com.giphy.viewer.domain.business.ImageInteractorImpl
import com.giphy.viewer.domain.repositories.ImagesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Natalia on 02.10.2018.
 */
@Module
class DomainModule {
    @Provides
    @Singleton
    fun provideImageInteractor(imagesRepository: ImagesRepository): ImageInteractor =
            ImageInteractorImpl(imagesRepository)
}