package com.giphy.viewer.di.modules

import com.giphy.viewer.di.scopes.PerFragment
import com.giphy.viewer.view.gallery.GalleryFragment
import com.giphy.viewer.view.image.ImageFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by natalia on 27.09.2018.
 */
@Module
abstract class FragmentBuilder {
    @PerFragment
    @ContributesAndroidInjector(modules = [PresentationModule::class])
    abstract fun provideGalleryFragment(): GalleryFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [PresentationModule::class])
    abstract fun provideImageFragment(): ImageFragment
}