package com.giphy.viewer.di.scopes

import javax.inject.Scope

/**
 * Created by Natalia on 02.10.2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity