package com.giphy.viewer.di

import com.giphy.viewer.TheApp
import com.giphy.viewer.di.modules.*
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    ActivityBuilder::class,
    AndroidSupportInjectionModule::class,
    DataModule::class,
    DomainModule::class
])
interface AppComponent : AndroidInjector<TheApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<TheApp>()
}