package com.giphy.viewer.di.modules

import android.content.Context
import com.giphy.viewer.TheApp
import com.giphy.viewer.utils.resources.StringManager
import com.giphy.viewer.utils.resources.StringManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by natalia on 29.09.2018.
 */
@Module
abstract class AppModule {
    @Binds
    @Singleton
    abstract fun providesAppContext(theApp: TheApp): Context

    @Binds
    @Singleton
    abstract fun providesStringManager(stringManagerImpl: StringManagerImpl): StringManager
}
