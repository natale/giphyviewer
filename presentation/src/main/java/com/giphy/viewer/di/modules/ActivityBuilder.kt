package com.giphy.viewer.di.modules

import com.giphy.viewer.di.scopes.PerActivity
import com.giphy.viewer.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by natalia on 26.09.2018.
 */

@Module
abstract class ActivityBuilder {
    @PerActivity
    @ContributesAndroidInjector(modules = [FragmentBuilder::class, DataModule::class])
    abstract fun bindMainActivity() : MainActivity
}