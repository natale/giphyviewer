package com.giphy.viewer.di.modules

import com.giphy.viewer.data.BuildConfig
import com.giphy.viewer.data.DataConfig
import com.giphy.viewer.data.api.GiphyApi
import com.giphy.viewer.data.api.KeyInterceptor
import com.giphy.viewer.data.models.ImageDto
import com.giphy.viewer.data.models.ImageResponse
import com.giphy.viewer.data.models.UrlDto
import com.giphy.viewer.data.utils.ImageResponseDeserializer
import com.giphy.viewer.data.utils.SingleImageDeserializer
import com.giphy.viewer.data.utils.UrlDeserializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        const val GSON_INNER = "inner"
        const val GSON_OUTER = "outer"
    }

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): GiphyApi = retrofit.create(GiphyApi::class.java)

    @Provides
    @Singleton
    @Named(GSON_OUTER)
    fun provideGson(@Named(GSON_INNER) gson: Gson): Gson = GsonBuilder()
            .registerTypeAdapter(ImageResponse::class.java, ImageResponseDeserializer(gson))
            .registerTypeAdapter(ImageDto::class.java, SingleImageDeserializer(gson))
            .create()

    @Provides
    @Singleton
    fun provideRetrofitClient(client: OkHttpClient, @Named(GSON_OUTER) gson: Gson): Retrofit =
            Retrofit.Builder()
                    .baseUrl(DataConfig.API_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .client(client)
                    .build()

    @Provides
    @Singleton
    fun provideClient(loggingInterceptor: HttpLoggingInterceptor, keyInterceptor: Interceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(keyInterceptor)
                    .addInterceptor(loggingInterceptor)
                    .build()

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor { message -> Timber.d(message) }.apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            }

    @Provides
    @Singleton
    fun provideKeyInterceptor(): Interceptor = KeyInterceptor()

    @Provides
    @Singleton
    @Named(GSON_INNER)
    fun provideInnerGson(): Gson = GsonBuilder()
            .registerTypeAdapter(UrlDto::class.java, UrlDeserializer())
            .create()
}