package com.giphy.viewer.di.modules

import com.giphy.viewer.di.scopes.PerFragment
import com.giphy.viewer.utils.ImageViewModelConverter
import com.giphy.viewer.utils.resources.StringManager
import dagger.Module
import dagger.Provides

/**
 * Created by Natalia on 30.09.2018.
 */
@Module
class PresentationModule {
    @Provides
    @PerFragment
    fun provideImageViewModelConverter(stringManager: StringManager) =
            ImageViewModelConverter(stringManager)
}