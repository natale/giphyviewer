package com.giphy.viewer.di.modules

import com.giphy.viewer.data.api.GiphyApi
import com.giphy.viewer.data.mappers.ImageConverter
import com.giphy.viewer.data.mappers.PageConverter
import com.giphy.viewer.data.repositories.ImagesRepositoryImpl
import com.giphy.viewer.domain.repositories.ImagesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by natalia on 28.09.2018.
 */
@Module
class DataModule {

    @Provides
    @Singleton
    fun provideImageConverter() = ImageConverter()

    @Provides
    @Singleton
    fun providePageConverter(imageConverter: ImageConverter) = PageConverter(imageConverter)

    @Provides
    @Singleton
    fun provideImageRepository(api: GiphyApi, imageConverter: ImageConverter, pageConverter: PageConverter): ImagesRepository =
            ImagesRepositoryImpl(api, imageConverter, pageConverter)
}