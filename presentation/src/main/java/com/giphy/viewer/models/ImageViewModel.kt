package com.giphy.viewer.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by natalia on 26.09.2018.
 */
@Parcelize
data class ImageViewModel(
        val url: String,
        val profileUrl: String,
        val userName: String,
        val displayName: String,
        val twitter: String) : Parcelable