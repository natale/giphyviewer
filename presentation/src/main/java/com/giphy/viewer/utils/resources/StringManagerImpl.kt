package com.giphy.viewer.utils.resources

import android.content.Context
import javax.inject.Inject

/**
 * Created by natalia on 29.09.2018.
 */
class StringManagerImpl @Inject constructor(private val context: Context): StringManager {
    override fun getString(resId: Int): String = context.getString(resId)
}
