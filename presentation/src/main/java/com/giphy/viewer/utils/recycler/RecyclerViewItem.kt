package com.giphy.viewer.utils.recycler

/**
 * Created by natalia on 29.09.2018.
 */
interface RecyclerViewItem {
    fun getLayout(): Int
}