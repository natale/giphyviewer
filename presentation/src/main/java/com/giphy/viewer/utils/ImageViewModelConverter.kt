package com.giphy.viewer.utils

import com.giphy.viewer.R
import com.giphy.viewer.domain.models.Image
import com.giphy.viewer.models.ImageViewModel
import com.giphy.viewer.utils.resources.StringManager
import com.giphy.viewer.view.gallery.recycler.ImageItem

/**
 * Created by Natalia on 30.09.2018.
 */
class ImageViewModelConverter(private val stringManager: StringManager) {

    fun convert(images: List<Image>): List<ImageItem> =
            images.asSequence()
                    .map { convertImage(it) }
                    .toList()

    fun convertImage(image: Image): ImageItem {
        val viewModel = ImageViewModel(image.url,
                image.author.profileUrl,
                getNotEmptyName(image.author.name),
                getNotEmptyName(image.author.displayName),
                image.author.twitter)
        return ImageItem(viewModel)
    }

    private fun getNotEmptyName(s: String): String =
            if (s.isEmpty()) stringManager.getString(R.string.default_user_name) else s
}