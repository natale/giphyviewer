package com.giphy.viewer.utils.recycler

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Natalia on 02.10.2018.
 */
interface BindableItem {
    fun bind(holder: RecyclerView.ViewHolder, clickListener: View.OnClickListener)
}