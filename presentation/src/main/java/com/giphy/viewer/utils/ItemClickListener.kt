package com.giphy.viewer.utils

/**
 * Created by natalia on 27.09.2018.
 */
interface ItemClickListener<T> {
    fun onItemClick(item: T)
}