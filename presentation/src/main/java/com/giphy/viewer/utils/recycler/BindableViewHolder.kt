package com.giphy.viewer.utils.recycler

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

/**
 * Created by natalia on 29.09.2018.
 */
class BindableViewHolder<T : ViewDataBinding>(val binding: T) : RecyclerView.ViewHolder(binding.root)