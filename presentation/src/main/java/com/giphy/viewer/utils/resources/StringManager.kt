package com.giphy.viewer.utils.resources

/**
 * Created by natalia on 29.09.2018.
 */
interface StringManager {
    fun getString(resId: Int): String
}