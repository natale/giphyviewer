package com.giphy.viewer.view.gallery.recycler

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.giphy.viewer.utils.ItemClickListener
import com.giphy.viewer.utils.recycler.BindableItem
import com.giphy.viewer.utils.recycler.BindableViewHolder
import com.giphy.viewer.utils.recycler.RecyclerViewItem

/**
 * Created by natalia on 29.09.2018.
 */
class GalleryAdapter(private val items: MutableList<RecyclerViewItem> = mutableListOf(),
                     private var listener: ItemClickListener<RecyclerViewItem>? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, layoutResId: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(inflater, layoutResId, parent, false)
        return BindableViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int) = items[position].getLayout()

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val image = items[position]
        if (image is BindableItem) {
            image.bind(viewHolder, View.OnClickListener { _ -> listener?.onItemClick(image) })
        }
    }

    fun setItems(list: List<RecyclerViewItem>) {
        val diffUtilCallback = ImageDiffUtilCallback(this.items, list)
        val diffResult = DiffUtil.calculateDiff(diffUtilCallback)
        diffResult.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(list)
    }

    fun addItems(list: List<RecyclerViewItem>) {
        val startPosition = items.size + 1
        items.addAll(list)
        notifyItemRangeInserted(startPosition, list.size)
    }

    fun setListener(listener: ItemClickListener<RecyclerViewItem>) {
        this.listener = listener
    }

    fun showLoading() = addAdditionalItem(LoadingItem())

    fun hideLoading() = removeAdditionalItem(LoadingItem::class.java)

    fun showError(message: String) = addAdditionalItem(ErrorItem(message))

    fun hideError() = removeAdditionalItem(ErrorItem::class.java)

    private fun removeAdditionalItem(type: Class<out RecyclerViewItem>) {
        if (items.isEmpty()) {
            return
        }

        val last = items.last()
        if (type.isInstance(last)) {
            items.remove(last)
            notifyItemRemoved(items.size)
        }
    }

    private fun addAdditionalItem(item: RecyclerViewItem) {
        if (items.isEmpty()) {
            return
        }

        val last = items.last()
        if (!item.javaClass.isInstance(last)) {
            items.add(item)
            notifyItemInserted(items.size)
        }
    }
}