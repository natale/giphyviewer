package com.giphy.viewer.view.gallery.recycler

import android.support.v7.widget.RecyclerView
import android.view.View
import com.giphy.viewer.R
import com.giphy.viewer.databinding.ItemListErrorBinding
import com.giphy.viewer.utils.recycler.BindableItem
import com.giphy.viewer.utils.recycler.BindableViewHolder
import com.giphy.viewer.utils.recycler.RecyclerViewItem

/**
 * Created by natalia on 29.09.2018.
 */
class ErrorItem(val message: String): RecyclerViewItem, BindableItem {

    @Suppress("UNCHECKED_CAST")
    override fun bind(holder: RecyclerView.ViewHolder, clickListener: View.OnClickListener) {
        val viewHolder = holder as BindableViewHolder<ItemListErrorBinding>
        viewHolder.binding.textView.text = message
        viewHolder.binding.btnRetry.setOnClickListener { clickListener.onClick(it) }
    }

    override fun getLayout(): Int = R.layout.item_list_error
}