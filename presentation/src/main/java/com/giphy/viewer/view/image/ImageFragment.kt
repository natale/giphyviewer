package com.giphy.viewer.view.image

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.giphy.viewer.R
import com.giphy.viewer.databinding.FragmentImageBinding
import com.giphy.viewer.models.ImageViewModel
import com.giphy.viewer.presenter.image.ImagePresenter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by natalia on 30.09.2018.
 */
class ImageFragment : MvpAppCompatFragment(), SingleImageView {

    companion object {
        const val KEY_IMAGE = "image"
        const val KEY_ID = "id"
    }

    @InjectPresenter
    lateinit var presenter: ImagePresenter

    @Inject
    lateinit var presenterProvider: Provider<ImagePresenter>

    private lateinit var binding: FragmentImageBinding

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_image, container, false)
        binding.presenter = presenter
        return binding.root
    }

    @ProvidePresenter
    fun providePresenter(): ImagePresenter {
        val presenter = presenterProvider.get()
        arguments?.let {
            val image: ImageViewModel? = it.getParcelable(KEY_IMAGE)
            val url = it.getString(KEY_ID)
            presenter.image = image
            presenter.deepLinkImageId = url
        }

        return presenter
    }

    override fun showImageDetails(imageViewModel: ImageViewModel) {
        binding.image = imageViewModel
    }

    override fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }

    override fun showLoading() {
        binding.pbLoader.visibility = View.VISIBLE
        binding.group.visibility = View.GONE
    }

    override fun hideLoading() {
        binding.pbLoader.visibility = View.GONE
        binding.group.visibility = View.VISIBLE
    }

    override fun showError(errorMessage: String) {
        Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.retry), { _ -> presenter.onRetryClick() })
                .show()
    }
}