package com.giphy.viewer.view.image

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.giphy.viewer.models.ImageViewModel

/**
 * Created by Natalia on 30.09.2018.
 */
interface SingleImageView : MvpView {
    fun showImageDetails(imageViewModel: ImageViewModel)

    @StateStrategyType(value = OneExecutionStateStrategy::class)
    fun openUrl(url: String)

    fun showLoading()

    fun hideLoading()

    fun showError(errorMessage: String)
}