package com.giphy.viewer.view.gallery

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.giphy.viewer.models.ImageViewModel
import com.giphy.viewer.view.gallery.recycler.ImageItem

/**
 * Created by natalia on 29.09.2018.
 */
interface GalleryView : MvpView {

    fun showImages(images: List<ImageItem>)

    fun updateImages(images: List<ImageItem>)

    @StateStrategyType(value = OneExecutionStateStrategy::class)
    fun showRefreshing()

    @StateStrategyType(value = OneExecutionStateStrategy::class)
    fun showRefreshError(errorMessage: String)

    @StateStrategyType(value = OneExecutionStateStrategy::class)
    fun showListLoading()

    fun showListError(errorMessage: String)

    fun hideLoading()

    fun hideError()

    @StateStrategyType(value = OneExecutionStateStrategy::class)
    fun openImageDetails(imageViewModel: ImageViewModel)
}