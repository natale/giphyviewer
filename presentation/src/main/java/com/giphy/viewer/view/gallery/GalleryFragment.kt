package com.giphy.viewer.view.gallery

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.giphy.viewer.R
import com.giphy.viewer.databinding.FragmentGalleryBinding
import com.giphy.viewer.models.ImageViewModel
import com.giphy.viewer.presenter.gallery.GalleryPresenter
import com.giphy.viewer.utils.recycler.SpaceItemDecoration
import com.giphy.viewer.view.gallery.recycler.GalleryAdapter
import com.giphy.viewer.view.gallery.recycler.ImageItem
import com.giphy.viewer.view.image.ImageFragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by natalia on 26.09.2018.
 */
class GalleryFragment : MvpAppCompatFragment(), GalleryView {

    companion object {
        const val IMAGE_SPAN_COUNT = 2
        const val SINGLE_ITEM_SPAN_COUNT = 1
        const val ROW_SPAN_COUNT = 2
    }

    @InjectPresenter
    lateinit var presenter: GalleryPresenter

    @Inject
    lateinit var presenterProvider: Provider<GalleryPresenter>

    private lateinit var binding: FragmentGalleryBinding
    private val galleryAdapter = GalleryAdapter()
    private var errorSnackBar: Snackbar? = null

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpList()
        binding.swipeRefreshLayout.setOnRefreshListener { presenter.onRefresh() }
    }

    @ProvidePresenter
    fun providePresenter(): GalleryPresenter = presenterProvider.get()

    override fun showImages(images: List<ImageItem>) {
        galleryAdapter.addItems(images)
    }

    override fun updateImages(images: List<ImageItem>) {
        galleryAdapter.setItems(images)
    }

    override fun showRefreshing() {
        binding.swipeRefreshLayout.isRefreshing = true
    }

    override fun showListLoading() {
        galleryAdapter.showLoading()
    }

    override fun hideLoading() {
        binding.swipeRefreshLayout.isRefreshing = false
        galleryAdapter.hideLoading()
    }

    override fun showListError(errorMessage: String) {
        galleryAdapter.showError(errorMessage)
    }

    override fun hideError() {
        galleryAdapter.hideError()
        errorSnackBar?.apply { dismiss() }
    }

    override fun showRefreshError(errorMessage: String) {
        errorSnackBar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(getString(R.string.retry), { _ -> presenter.onRetryRefreshClick() })
            show()
        }
    }

    override fun openImageDetails(imageViewModel: ImageViewModel) {
        val navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
        navController.navigate(R.id.imageFragment, Bundle().apply {
            putParcelable(ImageFragment.KEY_IMAGE, imageViewModel)
        })
    }

    private fun setUpList() {
        with(binding.recyclerView) {
            layoutManager = getGridLayoutManager()
            adapter = galleryAdapter
            val space = resources.getDimensionPixelOffset(R.dimen.rv_margin)
            addItemDecoration(SpaceItemDecoration(space))
            addOnScrollListener(createScrollListener())
        }

        galleryAdapter.setListener(presenter)
    }

    private fun getGridLayoutManager(): GridLayoutManager =
            GridLayoutManager(requireContext(), IMAGE_SPAN_COUNT).apply {
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int =
                            when (galleryAdapter.getItemViewType(position)) {
                                R.layout.item_list_image -> SINGLE_ITEM_SPAN_COUNT
                                else -> ROW_SPAN_COUNT
                            }
                }
            }

    private fun createScrollListener() =
            object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val layoutManager = (recyclerView.layoutManager as GridLayoutManager)
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()

                    presenter.onScrolled(visibleItemCount, totalItemCount, pastVisibleItems)
                }
            }
}