package com.giphy.viewer.view.gallery.recycler

import android.support.v7.util.DiffUtil
import com.giphy.viewer.utils.recycler.RecyclerViewItem

/**
 * Created by natalia on 29.09.2018.
 */
class ImageDiffUtilCallback(private val oldList: List<RecyclerViewItem>,
                            private val newList: List<RecyclerViewItem>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            checkItemsAreSame(oldItemPosition, newItemPosition)

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = true

    private fun checkItemsAreSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old = oldList[oldItemPosition]
        val new = newList[newItemPosition]
        if (old is ImageItem && new is ImageItem) {
            return old.image.url == new.image.url
        }

        return false
    }
}