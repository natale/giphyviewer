package com.giphy.viewer.view.gallery.recycler

import com.giphy.viewer.R
import com.giphy.viewer.utils.recycler.RecyclerViewItem

/**
 * Created by natalia on 29.09.2018.
 */
class LoadingItem : RecyclerViewItem {

    override fun getLayout() = R.layout.item_list_loading
}