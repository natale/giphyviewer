package com.giphy.viewer.domain.models

/**
 * Created by natalia on 28.09.2018.
 */
class User(val name: String,
           val displayName: String,
           val profileUrl: String,
           val twitter: String)