package com.giphy.viewer.domain.models

/**
 * Created by natalia on 28.09.2018.
 */
class Image(val url: String,
            val author: User)