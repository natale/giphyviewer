package com.giphy.viewer.domain.models

/**
 * Created by Natalia on 02.10.2018.
 */
class Page(val images: List<Image>, val maxCount: Int)