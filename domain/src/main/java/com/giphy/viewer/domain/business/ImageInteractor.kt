package com.giphy.viewer.domain.business

import com.giphy.viewer.domain.models.Image
import io.reactivex.Single

/**
 * Created by natalia on 28.09.2018.
 */
interface ImageInteractor {
    fun loadImages(offset: Int): Single<List<Image>>
    fun loadImage(id: String): Single<Image>
}