package com.giphy.viewer.domain.business

import com.giphy.viewer.domain.models.Image
import com.giphy.viewer.domain.repositories.ImagesRepository
import io.reactivex.Single

/**
 * Created by natalia on 28.09.2018.
 */
class ImageInteractorImpl(private val repository: ImagesRepository): ImageInteractor {

    companion object {
        const val PAGE_SIZE = 30
    }

    override fun loadImages(offset: Int): Single<List<Image>> {
            if (offset >= repository.getMaxImagesCount()) {
                return Single.just(listOf())
            }

            return repository.loadImages(offset, PAGE_SIZE)
                    .map { it.images }
    }

    override fun loadImage(id: String): Single<Image> =
            repository.loadImage(id)
}