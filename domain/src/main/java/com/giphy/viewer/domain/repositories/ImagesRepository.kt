package com.giphy.viewer.domain.repositories

import com.giphy.viewer.domain.models.Image
import com.giphy.viewer.domain.models.Page
import io.reactivex.Single

/**
 * Created by natalia on 28.09.2018.
 */
interface ImagesRepository {
    fun loadImages(offset: Int, count: Int): Single<Page>
    fun loadImage(id: String): Single<Image>
    fun getMaxImagesCount(): Int
}